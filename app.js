const htmlStandards = require('reshape-standard')
const cssStandards = require('spike-css-standards')
const jsStandards = require('spike-js-standards')
const pageId = require('spike-page-id')
const Contentful = require('spike-contentful')
const SpikeUtils = require('spike-util')
const locals = {}
const fs = require('fs')

// webpack plugin apply function
/*apply (compiler) {
  const util = new SpikeUtils(compiler.options)
}*/

module.exports = {
  plugins: [
    new Contentful({
      addDataTo: locals,
      accessToken: '437ea366f3584d9d1b1e234c547aba67b871beaa8e3e470f424b43ef1b30dac8',
      spaceId: 'f3aqvx0cqgye',
      includeLevel: 4,
      contentTypes: [
        {
          name: 'Home',
          id: 'home',
          filters: {
            limit: 2,
            include: 4
          },
          transform: (post) => {
            var language = post.fields !== undefined && post.fields.language !== undefined && post.fields.language.trim() !== '' ? post.fields.language : '';
            var item = {
              title: post.fields !== undefined && post.fields.heroImage !== undefined && post.fields.heroImage.fields.title !== undefined ? post.fields.heroImage.fields.title : '',
              image: post.fields !== undefined && post.fields.heroImage.fields.image !== undefined && post.fields.heroImage.fields.image.fields !== undefined && post.fields.heroImage.fields.image.fields.file !== undefined && post.fields.heroImage.fields.image.fields.file.url !== undefined ? post.fields.heroImage.fields.image.fields.file.url : '',
              mobileImage: post.fields !== undefined && post.fields.heroImage.fields.mobileImage !== undefined && post.fields.heroImage.fields.mobileImage.fields !== undefined && post.fields.heroImage.fields.mobileImage.fields.file !== undefined && post.fields.heroImage.fields.mobileImage.fields.file.url !== undefined ? post.fields.heroImage.fields.mobileImage.fields.file.url : '',
              language: language,
              links: {

              }
            }
            return item
          },
          template: {
            path: 'views/index.sml',
            output: (post) => {
              var language = post.language !== undefined ? post.language : ''
              console.log(language + ' hero image section')
              return language !== '' ? `views/` + language.toLowerCase() + `/index.html` : ''
            }
          }
        }
      ]
    })
  ],
  reshape: htmlStandards({
    locals
  }),
  postcss: cssStandards(),
  babel: jsStandards,
  node: {
    fs: "empty",
    net: 'mock',
    dns: 'mock'
  }
}
