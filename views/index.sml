doctype html
html
  head
    meta(charset='utf-8')
    meta(http-equiv='X-UA-Compatible' content='IE=edge')
    meta(name='viewport' content='width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=no')
    meta(name='description' content='')
    meta(name='author' content='')
    title Henrietta Hotel
    // Stylesheets
    // Bootstrap Core CSS
    link(href='assets/bootstrap/css/bootstrap.min.css' rel='stylesheet')
    // Custom Fonts
    link(href='assets/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css')
    // Theme CSS
    link(href='assets/css/style.css' rel='stylesheet')
    link(href='assets/css/animate.css' rel='stylesheet')
    link(href='assets/horzmenu/slick.css' rel='stylesheet')
    link(href='assets/daterangepicker/daterangepicker.css' rel='stylesheet')
    // HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
    // WARNING: Respond.js doesn't work if you view the page via file://
    //if lt IE 9
      script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
      script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
    // Scripts
    script(src='assets/jquery/jquery.min.js')
    script(src='assets/bootstrap/js/bootstrap.min.js')
    // Plugin JavaScript
    script(src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAnrWAlPZaNbl-wdCxceY7_uPlEQmg5l_M')
    script(src='assets/debouncer/jquery.hoverIntent.min.js')
    script(src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js')
    script(src='assets/horzmenu/slick.js')
    script(src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js' type='text/javascript')
    script(src='assets/daterangepicker/jquery.daterangepicker.js')

  body#page-top.index
    nav#mainNav.navbar.navbar-default.navbar-custom.navbar-fixed-top
      .navbar-header.page-scroll
        a#mob-nav.mob-menu(href='#')
          img(src='assets/img/mobile-menu.svg' alt='')
        .navbar-brand.navbar-brand-centered
          a.page-scroll(href='#page-top')
            img(src='assets/img/logo.svg' alt='Henrietta Hotel')
        #mob-booking.progress-btn.mob-btn.desk-mob
          a.btn(href='https://gc.synxis.com/rez.aspx?Hotel=77140&Chain=21856') BOOK NOW
      .mob-nav-cont
        .mob-nav-inner
          .mob-btm-cont
            img(src='assets/img/map-illustration2.png' alt='')
            .row
              .col-xs-12
                p
                  strong 14 15 Henrietta St.
                  br
                  |  WC2E 8QH LONDON
          ul
            li.has-links
              a.page-scroll(href='#rooms') rooms
              .swipe-menu.slider
                div
                  a(href='room-details.html#covent-garden') Covent Garden
                div
                  a(href='room-details.html#grand-covent-garden') Grand Covent Garden
                div
                  a(href='room-details.html#henrietta') Henrietta
                div
                  a(href='room-details.html#grand-henrietta') Grand Henrietta
            li
              a.page-scroll(href='#restaurants') restaurant & cocktails
            li
              a.page-scroll(href='#whattodo') what to do
            li
              a.page-scroll(href='#about') about us
            li
              a.page-scroll(href='#contact') contact
      #navbar-brand-centered.collapse.navbar-collapse
        ul#menu.nav.navbar-nav
          li
            a.page-scroll(href='#rooms')
              | rooms
              span.green-brdr
          li.dropdown
            a.page-scroll(href='#restaurants')
              | restaurant & cocktails
              span.green-brdr
          li
            a.page-scroll(href='#whattodo')
              | what to do
              span.green-brdr
          li
            a.page-scroll(href='#about')
              | about
              span.green-brdr
          li
            a.page-scroll(href='#contact')
              | contact
              span.green-brdr
        .nav.navbar-nav.navbar-right
          #four-inputs.booking-form
            #two-inputs
              #arrival-div.input-container
                input#date-range200.con_txt(type='text' required='' value='' tabindex='1' name='name')
                label(for='arrival') Arrival
              #departure-div.input-container
                input#date-range201.con_txt(type='text' required='' tabindex='2' value='' name='mobile')
                label(for='departure') Departure
            #room-det-menu.input-container
              input#room-det(type='text' required='')
              label(for='rooms') Rooms
              ul.dropdown-menu.rooms-dd
                li
                  h3 number of rooms
                  ul.rooms-list
                    li
                      a(href='#') 1
                    li
                      a(href='#') 2
            #adult-det-menu.input-container
              input#adult-det(type='text' required='')
              label(for='adults') Adults
              ul.dropdown-menu.adults-dd
                li
                  h3 number of adults
                  ul.adults-list
                    li
                      a(href='#') 1
                    li
                      a(href='#') 2
          .progress-btn
            a.book-btn(href='#') BOOK NOW

    #welcome-sec.section.active.animated.zoomOut
      header
        .header-cont.parallax
          //item.mobileImage
          img.welcome-img(src='{{ item.image }}') //item.mobileImage
          .intro-text
            .intro-heading {{ item.title }}
        .scroll-arw
          a.page-scroll(href='#rooms')
            img.anim-bounce(src='assets/img/scroll-arw.svg' alt='')

    #rooms.room-cont
      .room-cont-temp.animated
      .rooms
        // Rooms Section
        .container.roomsovw-mob
          .swipe-menu.center.slider
            div
              a(href='room-details.html#covent-garden') Covent Garden
            div
              a(href='room-details.html#grand-covent-garden') Grand Covent Garden
            div
              a(href='room-details.html#henrietta') Henrietta
            div
              a(href='room-details.html#grand-henrietta') Grand Henrietta
          .rooms-img-cont
            img(src='assets/img/mobile-rooms.jpg' alt='')
            .rooms-ovw
              h2
                | The Rooms
          .roomsovw-mob-cont
            p
              | Inspired by the surrounding streets of Covent Garden with a nod to Italian design, the 18 bedrooms & suites of The Henrietta Hotel are each unique, combining fabric panels, marble, carpet and hand built bedheads. Among the many services, guests are treated with 24-hour room service and mini bars carefully stocked with our favourite products, including house made cocktails.
        .container.roomsovw-dsktp
          .room-cont-middle.text-center
            h2.heading The Rooms
            p
              | Inspired by the surrounding streets of Covent Garden with a nod to Italian design, the 18 bedrooms & suites of The Henrietta Hotel are each unique, combining fabric panels, marble, carpet and hand built bedheads. Among the many services, guests are treated with 24-hour room service and mini bars carefully stocked with our favourite products, including house made cocktails.
          .room-cont-temp1.animated
          .room-cont-temp2.animated
          .room-cont-temp3.animated
          .room-cont-temp4.animated
          .room-cont-bottom
            a#rmcovent-garden.room-item(href='room-details.html#covent-garden')
              span
                | Covent Garden
                b Room
                img.rooms-arw(src='assets/img/rooms-hover.svg' alt='')
              p The perfect escape
            a#rmcovent-garden-grand.room-item(href='room-details.html#grand-covent-garden')
              span
                | Grand Covent Garden
                b Room
                img.rooms-arw(src='assets/img/rooms-hover.svg' alt='')
              p A view from the top
            a#rmhenrietta.room-item(href='room-details.html#henrietta')
              span
                | Henrietta
                b Room
                img.rooms-arw(src='assets/img/rooms-hover.svg' alt='')
              p Ultimate relaxation
            a#rmhenrietta-grand.room-item(href='room-details.html#grand-henrietta')
              span
                | Grand Henrietta
                b Room
                img.rooms-arw(src='assets/img/rooms-hover.svg' alt='')
              p Overlooking London

    #restaurants.t50
      .row.no-margin
        .col-xs-12.col-md-9.col-md-push-3.rest-img-sec
          h2
            | restaurant
            br
            | and cocktails
        .col-xs-12.col-md-3.col-md-pull-9.rest-cont-sec
          span.contact-green-border
          h4 14 - 15 Henrietta Street
          p
            | WC2E 8QH London
            br
          span.contact-green-border
          p.mpt20
            | The menu, by Ollie Dabbous, is ingredient-led with simple, seasonal dishes and subtle accent of France throughout. The cocktail menu conceived by drink historians Jared Brown & Anistatia Miller is a journey of the location's storied past.
          p
            | Paying tribute to the former gardens of Covent Garden, a glass roof encapsulates the mezzanine of Henrietta. Terracotta, oak and marble sit alongside flora & fauna atmosphere to delight the senses.
          .rest-cont-sec-btm
            span.contact-green-border
            h4 Restaurant Reservations
            p
              a(href='mailto:eat@henriettahotel.com?subject=New Booking Request for Henrietta Hotel&body=Hello%0D%0A%0D%0AThanks for your interest in dining at Henrietta Hotel. Please let us know the date and time you would like to join us, and the number of guests in your party. We look forward to seeing you! %0D%0A %0D%0AName: %0D%0A%0D%0ADate:%0D%0A%0D%0AGuests:%0D%0A%0D%0ASpecial Requests:%0D%0A%0D%0AEmail:%0D%0A%0D%0ATelephone:%0D%0A%0D%0A%0D%0A%0D%0ALove,%0D%0AHenrietta xxx')  eat@henriettahotel.com
            p
              | Please note we don't take reservations for our bar areas.
              br
              | Do drop by and join us for a drink.

    #whattodo
      .whattodo-sec1
        #todo-content
          .todocont-inner.text-center
            h2
              | Covent Garden
              span - stay with us at the heart of london -
            p
              | A bold history rich in art, literature and music, Henrietta Street dates back to 1634 noting painter Samuel Scott and actress Kitty Clive as former residents.
            p
              | Henrietta Hotel & Restaurant combines two townhouses at numbers 14 & 15 Henrietta Street; both with their own varied histories. No. 14 Henrietta Street once housed the office of Victor Gollancz Ltd, who published George Orwell, Kingsley Amis and John Le Carré among others. No. 15 Henrietta Street, designed by HE Pollard, is an Anglo-Dutch style featuring red brick and terracotta.
            p
              | Today, The Henrietta Hotel & Restaurant at 14-15 Henrietta Street is an exceptional location where guests are invited to stay in our bedrooms, dine in our restaurant or drink at our bar.
            .hidden-xs.local-attractions
              h4 LOCAL ATTRACTIONS
              ul.google-map-categories
                li.category-eat.active(data-location-category='eat')
                  span
                    img.eaticn(src='assets/img/map-icns/eat.png' alt='')
                    img.eaticn-act(src='assets/img/map-icns/Eat_mobile_hover.png' alt='')
                    i To Eat
                li.category-drink.active(data-location-category='drink')
                  span
                    img.drinkicn(src='assets/img/map-icns/drink.png' alt='')
                    img.drinkicn-act(src='assets/img/map-icns/Drink_hover.png' alt='')
                    i To Drink
                li.category-shop.active(data-location-category='shop')
                  span
                    img.shopicn(src='assets/img/map-icns/shop.png' alt='')
                    img.shopicn-act(src='assets/img/map-icns/Shop_hover.png' alt='')
                    i To Shop
                li.category-see.active(data-location-category='see')
                  span
                    img.seeicn(src='assets/img/map-icns/see.png' alt='')
                    img.seeicn-act(src='assets/img/map-icns/See_hover.png' alt='')
                    i To see
          .todo-map-scrl-links.hidden-xs
            ul.google-map-categories
              li.category-eat.active(data-location-category='eat')
                span
                  img.eaticn(src='assets/img/map-icns/eat.png' alt='')
                  img.eaticn-act(src='assets/img/map-icns/Eat_mobile_hover.png' alt='')
                  | To Eat
              li.category-drink.active(data-location-category='drink')
                span
                  img.drinkicn(src='assets/img/map-icns/drink.png' alt='')
                  img.drinkicn-act(src='assets/img/map-icns/Drink_hover.png' alt='')
                  | To Drink
              li.category-shop.active(data-location-category='shop')
                span
                  img.shopicn(src='assets/img/map-icns/shop.png' alt='')
                  img.shopicn-act(src='assets/img/map-icns/Shop_hover.png' alt='')
                  | To Shop
              li.category-see.active(data-location-category='see')
                span
                  img.seeicn(src='assets/img/map-icns/see.png' alt='')
                  img.seeicn-act(src='assets/img/map-icns/See_hover.png' alt='')
                  | To see
        .google-map
        .todo-map-links-mob.visible-xs
          ul.google-map-categories
            li.category-eat.active(data-location-category='eat')
              span
                img.eaticn(src='assets/img/map-icns/eat.png' alt='')
                img.eaticn-act(src='assets/img/map-icns/Eat_mobile_hover.png' alt='')
                | To Eat
            li.category-drink.active(data-location-category='drink')
              span
                img.drinkicn(src='assets/img/map-icns/drink.png' alt='')
                img.drinkicn-act(src='assets/img/map-icns/Drink__mobile_hover.png' alt='')
                | To Drink
            li.category-shop.active(data-location-category='shop')
              span
                img.shopicn(src='assets/img/map-icns/shop.png' alt='')
                img.shopicn-act(src='assets/img/map-icns/Shop_mobile_hover.png' alt='')
                | To Shop
            li.category-see.active(data-location-category='see')
              span
                img.seeicn(src='assets/img/map-icns/see.png' alt='')
                img.seeicn-act(src='assets/img/map-icns/See_mobile_hover.png' alt='')
                | To see
        .map-info-cont.animated
    #about
      section.about-cont
        .about-div
          img.about-mob(src='assets/img/henrietta-about-mobile.jpg' alt='')
          .text-center.about-text
            .after-exp-club
              h3.about-head - WE ARE JUST A BUNCH OF FRIENDS -
              p
                | Henrietta Hotel is the first London-based hotel from the collective behind Experimental Group. Romée de Goriainoff, Pierre-Charles Cros, Olivier Bon and Xavier Padovani have watched Covent Garden grow with some of London's best restaurants, award-winning bars, theaters and museums.
              p
                | The 18 bedrooms, designed by Dorothée Meilichzon (CHZON), weaves the collective's passion for food, wine, cocktails and design. It joins the fold with a new restaurant led by Michelin starred chef Ollie Dabbous.
              p
                | Henrietta Hotel is a new kind of locale bathed in Covent Garden's rich history whilst taking influence from London's heady energy.
        footer.footer.about-footer
          p.footer-title-text.text-center  - OUR HOTELS -
          .row.about-footer-img-div
            .col-sm-4.col-xs-12.col-lg-4.about-footer-group.acts-as-link(data-url='http://www.grandpigalle.com/')
              .hotel-title
                p
                  | GRAND PIGALLE
                  br
                  span HOTEL
              .group-logo-div
                img.group-logo.gl-img1(src='assets/img/grand_pigalle_logo.svg')
                img.group-logo.group-logo-hover(src='assets/img/grand_pigalle_logo_hover.svg')
              .footer-script-text
                p Paris
            .col-sm-4.col-xs-12.col-lg-4.about-footer-mdiv
              .hotel-title
                p
                  | HENRIETTA
                  br
                  span HOTEL
              .group-logo-div
                img.group-logo(src='assets/img/henrietta_logo_building.svg')
              .footer-script-text
                p London - 2017
            .col-sm-4.col-xs-12.col-lg-4
              .hotel-title
                p
                  | GRAND BOULEVARD
                  br
                  span HOTEL
              .group-logo-div
                img.group-logo(src='assets/img/grand_boulevard_logo.svg')
              .footer-script-text
                p Paris - 2017
          p.footer-title-text.text-center  - OUR LONDON BARS -
          .row.about-footer-img-div
            .col-sm-4.col-xs-12.col-lg-4.compagnie-logo.acts-as-link(data-url='http://www.cvssevendials.com/')
              .group-logo-div
                img.group-logo.gl-compagnie(src='assets/img/compagnie-des-vins.svg')
            .col-sm-4.col-xs-12.col-lg-4.about-footer-mdiv.ecc-logo.acts-as-link(data-url='http://experimentalcocktailclublondon.com/')
              .group-logo-div
                img.group-logo.gl-ecc(src='assets/img/ecc-london.png')
            .col-sm-4.col-xs-12.col-lg-4.joyeux-logo.acts-as-link(data-url='http://www.joyeuxbordel.com/')
              .group-logo-div
                img.group-logo.gl-joyeux(src='assets/img/joyeux-bordel.png')
    #contact.t50
      .pt30.contact-cont
        .col-xs-12.col-md-9.col-md-push-3.contact-right
          #contact-map
        .col-xs-12.col-md-3.col-md-pull-9.contact-left
          p.contact-title-text-bold.mt30 14 - 15 Henrietta Street
          p.contact-title-text WC2E 8QH London
          p.contact-title-text-bold
            a.rd-getd(href='direction-overlay.html') GET DIRECTIONS
          span.contact-green-border
          p
            | Henrietta Hotel is located just off of Covent Garden Market and close to The Strand. On our doorstep, guests on a culinary escapade will have access to some of London’s best restaurants and bars, and for those in search of London’s great cultural offerings, they will just a stroll from theatres, cinemas, museums and shopping.In addition to staying, dining or drinking with us, we can also host private receptions, events and meetings.
          h3 Hotel Reservations
          p.contact-title-text
            strong E
            a(href='mailto:sleep@henriettahotel.com?subject=New Booking Request for Henrietta Hotel&body=Hello%0D%0A%0D%0AThanks for your interest in staying with us at Henrietta Hotel. If you could please let us know your stay details, we will come back to you with availability. %0D%0A %0D%0AName: %0D%0A%0D%0AArrival Date:%0D%0A%0D%0ADeparture Date:%0D%0A%0D%0AGuests:%0D%0A%0D%0ASpecial Requests:%0D%0A%0D%0AEmail:%0D%0A%0D%0ATelephone:%0D%0A%0D%0A%0D%0A%0D%0ALove,%0D%0AHenrietta xxx')  sleep@henriettahotel.com
          h3 Restaurant Reservations
          p.contact-title-text
            strong E
            a(href='mailto:eat@henriettahotel.com?subject=New Booking Request for Henrietta Hotel&body=Hello%0D%0A%0D%0AThanks for your interest in dining at Henrietta Hotel. Please let us know the date and time you would like to join us, and the number of guests in your party. We look forward to seeing you! %0D%0A %0D%0AName: %0D%0A%0D%0ADate:%0D%0A%0D%0AGuests:%0D%0A%0D%0ASpecial Requests:%0D%0A%0D%0AEmail:%0D%0A%0D%0ATelephone:%0D%0A%0D%0A%0D%0A%0D%0ALove,%0D%0AHenrietta xxx')  eat@henriettahotel.com
          h3 Meeting & Event enquiries
          p.contact-title-text
            strong E
            a(href='mailto:julie@henriettahotel.com?subject=New Booking Request for Henrietta Hotel&body=Hello%0D%0A%0D%0AThank you for your interest in hosting your event with us at Henrietta Hotel. Please let us know the date of your event, number of guests and any other details so we can come back to you with availability. %0D%0A %0D%0AName: %0D%0A%0D%0AEvent Date:%0D%0A%0D%0AGuests:%0D%0A%0D%0ASpecial Requests:%0D%0A%0D%0AEmail:%0D%0A%0D%0ATelephone:%0D%0A%0D%0A%0D%0A%0D%0ALove,%0D%0AHenrietta xxx')  julie@henriettahotel.com
            .btm-txt
              | Love, Henrietta xx
      .text-center.mtb30
        form#email-form.input-container.input-email.show-before-submission(name='emailForm' method='post' action='' autocomplete='off')
          button#email-submit.btn.btn-default.email-btn(type='submit')
            span.glyphicon.glyphicon-chevron-right
          input#user-email.inp-email(type='text' required='' value='' tabindex='1' name='email')
          label.email-capt(for='arrival') Enter your email to hear more
          label#email-submission-error-text.email-submission-error-text.form-validation-error-margin.p-margin.hide-on-load.center-text Please enter an email address
        label.thanks-msg.center-text.grey-script-text.hide-on-load.show-after-submission Thanks! We'll be in touch soon.
        p.contact-input-tnc-text
          | By entering your email, you agree to our
          a(href='tnc.html')  Terms & Conditions
          |  and that you have read our
          a(href='privacy.html')  Privacy Policy
          | , including our
          a(href='cookie.html') Cookies Use
          | .
        .social-icon-div
          a(href='https://www.instagram.com/henriettahotel/' target='_blank')
            img.social-icon.insta-inactive(src='assets/img/Icon_Insta_Inactive.svg')
          a(href='https://www.facebook.com/henriettahotel' target='_blank')
            img.social-icon.fb-inactive(src='assets/img/Icon_Facebook_Inactive.svg')
          a(href='https://www.linkedin.com/company-beta/11057870/' target='_blank')
            img.social-icon.li-inactive(src='assets/img/Icon_Ilinkedin_Inactive.svg')
        .navbar.nav.direction-nav-link
          li
            a.page-scroll(href='#contact') CONTACT
          li
            a.page-scroll(href='tnc.html') TERMS & CONDITIONS
          li
            a.page-scroll(href='privacy.html') PRIVACY POLICY
          li
            a.page-scroll(href='cancellation.html') CANCELLATION POLICY
        p.contact-input-tnc-text Interior images by Paul Bowyer © 2016 The Experimental Group
